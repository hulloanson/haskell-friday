module Main where


import Lib

-- prefix form
(##) a b = a * b

-- infix form
a %% b = a + b

(ab) n c = n / c

main :: IO ()
main = putStrLn $ show (2 ## 5) ++ "\n" ++ show (2 %% 5) ++ "\n" ++ show (2 `ab` 5) ++ "\n" ++ show (ab 2 5)

