module Main where

import Lib

main :: IO ()
-- main = print (third [1,3,4])
main = print (third "124")

third :: [a] -> a

third [_, _, a] = a
