module Main where

import Lib

add :: Int -> Int -> Int

add = \x -> (\y -> x + y)

-- add x y = x + y

main :: IO ()
main = putStrLn (show (add 10 12))

