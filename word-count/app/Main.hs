module Main where

import Lib

main = interact lineCount
  where lineCount input = show (length (lines input)) ++ "\n" -- ++ was string concatenation

{--
  Dissecting (actually guessing) the program above:
  in the entrypoint main:
  - apply the function interact on lineCount. interact should get what user types
    - lineCount is a function which takes input as an argument. it should print number of lines in the input
      - show is a function that converts value to a string and print it to console
        - length counts the length of a list
          - lines is a function that split a string into a list by the newline '\n' character and returns it.
  Yay! I was right.
  Some additional notes after running this program:
  It's going to run right away after `stack run`. Write a few lines of sentences. Press "ctrl+d" to finish

  I actually missed one detail:

  main, the entry point need to return an IO. show however returns a String. to satisfy that, interact is actually of type:

  (String -> String) -> IO ()

  It means it takes a function that map a String to a String and returns an IO. Now main is a function that returns IO.

  so by new reckoning, interact actually takes input as String form console, supplies the String to the (String -> String) function supplied to it, and print the String obtained from the function to console.

  lineCount is a function that returns number of lines as a string.
--}
