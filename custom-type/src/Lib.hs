module Lib
    ( Book(), -- export type `BookInfo` with constructor `Book`
    getAuthors, getISBN, getTitle,
    ) where

-- Create a new type
-- BookInfo: type constructor
-- Book: value constructor - constructs a value of type BookInfo
-- `data` means it's an "algebraic data type"
-- data BookInfo = Book Int String [String]
--                 deriving (Show) -- for ghci to print the type

data Book = Book Int String [String]
                deriving (Show) -- for ghci to print the type

-- on ghci:
-- *Lib> :type Book
-- Book :: Int -> String -> [String] -> BookInfo

-- The type constructor and value constructor can have the same name -
-- and it's customary for them to be the same
-- so for real world application, it's more like this:
data Product = Product Int String
                deriving (Show)

-- How to access fields of an algebraic data type?
-- we use "deconstruction" (pattern matching with a value constructor)
getISBN :: BookInfo -> Int

getISBN (Book isbn _ _) = isbn

getTitle :: BookInfo -> String

getTitle (Book _ title _) = title

-- getAuthors :: Book -> [String] -- of course type signature is optional here

getAuthors (Book _ _ authors) = authors